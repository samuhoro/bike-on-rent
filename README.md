# Bike On Rent
### This repository contains the source code of an application name Bike On Rent.
 Designing a website for bikes and riding equipments on rent so that customers can check for availabilities.  Customers can search and book bikes according to requirement.They can book bikes on hour/day/week/monthly basis. They can pay the rent amount online and confirm there booking. If somehow they change there mind and want to cancel booking for that we provide a cancelation policy for cancel their booking.  Driving licence must be required for booking.

Live App:- https://bike-on-rent.firebaseapp.com