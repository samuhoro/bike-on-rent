import React from 'react';
import { Form, FormGroup, Label, Col, Input, Button } from 'reactstrap';
import Avatar from 'react-avatar';



const id = document.getElementById('group_image');

class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isEdit: false,
            profile: "https://placeholdit.imgix.net/~text?txtsize=18&txt=100%C3%97100&w=100&h=100",
            name: "",
            mobileNo: "",
            dob: "",
            bloodGroup: "",
            occupation: "",
            fullAddress: ""
        }
    }

    toggleEdit = () => {
        this.setState({
            isEdit: !this.state.isEdit
        })
    }


    editProfileHandler = () => {
        alert("ProfileUrl : " + this.state.profile + "\nMobile Number : " + this.state.mobileNo + "\nDOB : " + this.state.dob + "\nBlood Group : " + this.state.bloodGroup + "\nOccupation : " + this.state.occupation + "\nFull Address : " + this.state.fullAddress)
    }

    inputHandler = (event) => {
        let name = event.target.name;
        let value = event.target.value;

        this.setState({
            [name]: value
        })
    }

    onImageChange(event) {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => {
                this.setState({ profile: e.target.result });
            };
            reader.readAsDataURL(event.target.files[0]);
        }
    }


    handleClick = (e) => {
        var inputField = this.refs.fileField;
        if (this.state.isEdit) {
            inputField.click();
        }
    }

    render() {
        return (
            <div className="container col-6 border mt-2 rounded shadow-lg p-3 mb-5 bg-white" >
                <div className="row row-content">
                    <div className="col-12 font-lg textCenter">
                      {this.state.isEdit?"Edit ":null}  Your Profile <span className="fa fa-edit fa-lg mousePointer" onClick={this.toggleEdit}></span>
                    </div>
                </div>
                <Form>
                    <FormGroup row>
                        <Col md={12} className="textCenter">
                            <div id="test" onClick={this.handleClick}>
                                <img src={this.state.profile} id="profilePic" name="profilePic" height="100" width="100" alt="profile" className="rounded-circle" />
                                <input ref="fileField" hidden type="file" onChange={this.onImageChange.bind(this)} name="image1" accept=".jpg,.jpeg,.png" id="img1" />

                            </div>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label md={4}>Name </Label>
                        <Col md={8}>
                            <Input type="text" value={this.state.name} name="name" placeholder="Name" onChange={(event) => this.inputHandler(event)} readOnly={!this.state.isEdit} />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label md={4}>Mobile Number </Label>
                        <Col md={8}>
                            <Input type="text" value={this.state.mobileNo} name="mobileNo" placeholder="Mobile Number" onChange={(event) => this.inputHandler(event)}
                                readOnly={!this.state.isEdit} />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label md={4}>DOB </Label>
                        <Col md={8}>
                            <Input type="text" value={this.state.dob} name="dob" placeholder="Date Of Birth" onChange={(event) => this.inputHandler(event)}
                                readOnly={!this.state.isEdit} />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label md={4}>Blood Group </Label>
                        <Col md={8}>
                            <Input type="text" value={this.state.bloodGroup} name="bloodGroup" placeholder="Blood Group" onChange={(event) => this.inputHandler(event)}
                                readOnly={!this.state.isEdit} />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label md={4}>Occupation</Label>
                        <Col md={8}>
                            <Input type="text" value={this.state.occupation} name="occupation" placeholder="Occupation" onChange={(event) => this.inputHandler(event)}
                                readOnly={!this.state.isEdit} />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label md={4}>Full Address</Label>
                        <Col md={8}>
                            <Input type="textarea" value={this.state.fullAddress} rows="6" name="fullAddress" placeholder="Full Address" onChange={(event) => this.inputHandler(event)}
                                readOnly={!this.state.isEdit} />
                        </Col>
                    </FormGroup>
                    {this.state.isEdit ?
                        <FormGroup row>
                            <Col md={{ sige: 8, offset: 4 }}>
                                <Button onClick={this.editProfileHandler} color="primary">Save</Button>
                            </Col>
                        </FormGroup> : null}
                </Form>
            </div>
        )
    }
}

export default Profile;