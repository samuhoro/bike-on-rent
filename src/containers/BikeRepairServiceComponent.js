import React from 'react';
import {
    Label, Card, CardImg, CardText, CardBody,
    CardTitle
} from 'reactstrap';

import { SERVICES } from '../shared/services';

class BikeRepairService extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedRoute: "2",
            services: SERVICES
        }
    }

    selectHandler = (event) => {
        this.setState({
            selectedRoute: event.target.value
        })
    }



    render() {
        return (
            <div className="container">
                <div className="row ml-auto textCenter">
                    <p style={{ fontSize: 18 }}><strong>Bike Repair Service</strong></p>
                </div>
                <div className="row justify-content-center">
                    <Label md={1}>Route</Label>
                    <select className="form-control col-sm-3" onChange={this.selectHandler} value={this.state.selectedRoute}>
                        <option value="1">Ranchi to Jamshedpur</option>
                        <option value="2">Jamshedpur to Bokaro</option>
                        <option value="3">Bokaro to Dhanbad</option>
                        <option value="4">Dhanbad to Ranchi</option>
                    </select>
                </div>
                <div className="row justify-content-center">
                    <RepairList services={this.state.services.filter(service => service.routeId === parseInt(this.state.selectedRoute))} />
                </div>
            </div>
        )
    }
}

const RepairList = ({ services }) => {
    return services.map(service => {
        return (
            <div className="col-sm-3 m-2" key={service.id}>
                <Card>
                    <CardImg top width="80%" src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180" alt="Card image cap" />
                    <CardBody>
                        <CardTitle>{service.repairShopName}</CardTitle>
                        <p><strong>Timing : </strong>{service.timing}</p>
                        <p><strong>Contact : </strong> {service.contact}</p>
                        <CardText><strong>Address : </strong>{service.address}</CardText>
                    </CardBody>
                </Card>
            </div>
        )
    }
    )

}

export default BikeRepairService;