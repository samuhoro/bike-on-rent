import React from 'react';

import { EquipmentList } from '../shared/equioments';

import EquipmentCard from '../components/EquipmentCardComponent';

class Equipments extends React.Component {   
    bookingHandler=(bikeId)=>{
        this.props.history.push("/bikeBooking")
    }
    render() {

        const  equipmentList = EquipmentList.map(equipment => {
            return (
                <EquipmentCard equipment={equipment} key={equipment.id} routeToBooking={(bikeId)=> this.bookingHandler(bikeId)}/>
            )
        })

        return (
            <div className="container">
                <div className="row">
                    <p style={{ fontSize: 18 }}><strong>Equipments</strong></p>
                </div>
                <div className="row">
                    {equipmentList}
                </div>
            </div>
        )
    }
}

export default Equipments;