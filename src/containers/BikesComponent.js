import React from 'react';
import {Modal, ModalBody } from 'reactstrap';

import { BikeList } from '../shared/bikes';

import BikeCard from '../components/BikeCardComponent';



class Bikes extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false,
            bikeId: null,
            bikeDetails: null,
            bikes: Bikes
        }
    }

    toggleModal = (bikeId) => {
        const bikeDetails = BikeList.filter(bike => bike.id === bikeId)[0];
        this.setState({
            isModalOpen: !this.state.isModalOpen,
            bikeId: bikeId,
            bikeDetails: bikeDetails
        })
    }

    bookingHandler=(bikeId)=>{
        this.props.history.push("/bikeBooking")
    }

    render() {
        const bikesList = BikeList.map(bike => {
            return (
                <BikeCard className="mousePointer" bike={bike} key={bike.id} onclick={(bikeId) => this.toggleModal(bikeId)} routeToBooking={(bikeId)=> this.bookingHandler(bikeId)}/>
            )
        })

        return (
            <React.Fragment>
                <div className="container">
                    <div className="row">
                        <p style={{ fontSize: 18 }}><strong>Bikes</strong></p>
                    </div>
                    <div className="row">
                        {bikesList}
                    </div>

                </div>
                {this.state.bikeDetails ?
                    <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                        <ModalBody>
                            <div className="row">
                                <div className="col-sm-6">
                                    <img src={"/assets/images/bikes/" + this.state.bikeId + ".jpg"} alt={"nnb"} height="250" width="250" />
                                </div>
                                <div className="col-sm-6">
                                    <h3>{this.state.bikeDetails.bikeModel}</h3>

                                    <p>Registration No. : {this.state.bikeDetails.details.registration}</p>
                                    <p>Puchase Year     : {this.state.bikeDetails.details.purchaseYear}</p>
                                    <p>Mileage/CC        : {this.state.bikeDetails.details.mileage + "kmpl/" + this.state.bikeDetails.details.cc + "CC"}</p>
                                    <p>Last Servicing   : {this.state.bikeDetails.details.lastServicing}</p>
                                    <p>Runned           : {this.state.bikeDetails.details.km + "km"}</p>

                                </div>
                            </div>
                        </ModalBody>
                    </Modal> : null}
            </React.Fragment>
        )
    }
}

export default Bikes;