import React from 'react';
import { Label, Button } from 'reactstrap';
import CardTemplate from '../components/CardTemplateComponent';

class Home extends React.Component {
    clickHandler = (values) => {
        this.props.history.push("/"+values);
    }

    render() {
        return (
            <div className="container">
                <div className="row justify-content-center m-5">
                    <Label md={1}>City</Label>
                    <select className="form-control col-sm-3" >
                        <option>Ranchi</option>
                        <option>Jamshedpur</option>
                        <option>Bokaro</option>
                        <option>Dhanbad</option>
                    </select>
                    <Button className="ml-1"><span className="fa fa-search fa-lg"></span></Button>
                </div>
                <div className="row textCenter">
                    <div className="col-sm-3 mousePointer">
                        <CardTemplate type="Bikes" onclick={(values) => this.clickHandler(values)}/>
                    </div>
                    <div className="col-sm-6">

                    </div>
                    <div className="col-sm-3 mousePointer">
                        <CardTemplate type="Equipments" onclick={(values)=>this.clickHandler(values)}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home;
