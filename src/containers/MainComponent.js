import React from 'react';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';

import Header from '../components/HeaderComponent';
import Footer from '../components/FooterComponent';
import Home from './HomeComponent';
import Bikes from './BikesComponent';
import Equipments from './EquipmentsComponent';
import Feedback from './FeedbackComponent';
import BikeBooking from './BikeBookingComponent';
import BikeRepairService from './BikeRepairServiceComponent';
import Profile from './ProfileComponent';
import CityGuide from './CityGuideComponent';


class Main extends React.Component {



    render() {
        return (
            <React.Fragment>
                <Header {...this.props}/>
                <Switch>
                    <Route exact path="/home" component={Home} />
                    <Route path="/bikeRepair" component={BikeRepairService} />
                    <Route path="/cityGuide" component={CityGuide} />
                    <Route path="/firstAid" render={() => <h1>First Aid Comming Soon</h1>} />
                    <Route path="/profile" component={Profile} />
                    <Route path="/feedback" component={Feedback} />
                    <Route path="/bikes" component={Bikes} />
                    <Route path="/equipments" component={Equipments} />
                    <Route path="/bikeBooking" component={BikeBooking} />
                    <Route path="/logout" render={() => <h1>Logout Comming Soon</h1>} />
                    <Redirect to="/home" />
                </Switch>
                <Footer />
            </React.Fragment>
        )
    }
}

export default withRouter(Main);