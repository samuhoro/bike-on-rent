import React from 'react';
import { Button, Label, Col, Row } from 'reactstrap';
import { Control, Form, Errors } from 'react-redux-form';

const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => val && (val.length >= len);
const isNumber = (val) => !isNaN(Number(val));

class Feedback extends React.Component {

    handleSubmit = (values) => {
            alert("Booking Id : " + values.bookingId + "\nRating : " + values.rating + "\nYour Feedback : " + values.message)
    }

    render() {
        return (
            <div className="container col-6 border mt-2 rounded shadow-lg p-3 mb-5 bg-white">
                <div className="row row-content ">
                    <div className="col-12 textCenter">
                        <h3>Your Feedback</h3>
                    </div>
                    <div className="col-12 justify-content-center">
                        <Form model="feedback" onSubmit={(values) => this.handleSubmit(values)}>

                            <Row className="form-group">
                                <Label htmlFor="bookingId" md={4}>Booking Id : </Label>
                                <Col md={8}>
                                    <Control.text model=".bookingId" id="bookingId" name="bookingId" placeholder="Your Booking ID"
                                        className="form-control"
                                        validators={{
                                            required, minLength: minLength(15), maxLength: maxLength(15), isNumber
                                        }} />
                                    <Errors className="text-danger"
                                        model=".bookingId"
                                        show="touched"
                                        messages={{
                                            required: 'Required. ',
                                            minLength: 'Must be 15 digit. ',
                                            maxLength: 'Must be 15 digit. ',
                                            isNumber: 'Must be a number. '
                                        }} />
                                </Col>
                            </Row>

                            <Row className="form-group">
                                <Label htmlFor="rating" md={4}>Rating</Label>
                                <Col md={8}>
                                    <Control.select
                                        model=".rating"
                                        name="rating"
                                        className="form-control"
                                        defaultValue="1"
                                    >
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </Control.select>
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Label htmlFor="feedback" md={4}>You Feedback</Label>
                                <Col md={8}>
                                    <Control.textarea model=".message" id="message" name="message" rows="6 "
                                        className="form-control"
                                        validators={{
                                            required
                                        }} />
                                    <Errors className="text-danger"
                                        model=".message"
                                        show="touched"
                                        messages={{
                                            required: 'Required. '
                                        }} />
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Col md={{ size: 10, offset: 2 }}>
                                    <Button type="submit" color="primary">
                                        Send Feedback
                                    </Button>
                                </Col>
                            </Row>
                        </Form>
                    </div>
                </div>
            </div>
        )
    }
}

export default Feedback;