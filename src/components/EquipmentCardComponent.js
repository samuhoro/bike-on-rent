import React from 'react';
import {  Card, CardTitle, CardBody  } from 'reactstrap';


const Equipment = (props) =>{

  const bookHandler=()=>{
    props.routeToBooking(props.equipment.id)
  }

    return (
      <div className="col-sm-4 col-md-3 mt-4">
        <Card>
        <CardTitle className="textCenter">{props.equipment.name}</CardTitle>
          <img width="100%" src={"/assets/images/equipments/"+props.equipment.images[props.equipment.id-1]} alt={props.equipment.name} />
          <CardBody>
          <strong>{props.equipment.price + " Rs/Day"}</strong>
          <button className="ml-2 btn btn-primary" onClick={()=>bookHandler()}>Book</button>
          </CardBody>
        </Card>
      </div>
    );

  }


export default Equipment;