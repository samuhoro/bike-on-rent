import React from 'react';

function Footer(props) {
    return (
        <div className="footer pt-5">
            <div className="container textCenter">
                <div className="row justify-content-center">
                    <div className="col-auto">
                        <p>Copyright © 2018 Bike On Rent All Rights Reserved!<br/>Designed By - Samu Horo</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer;