import React from 'react';
import {  Card, CardBody, CardLink  } from 'reactstrap';


const CardTemplate = (props) =>{

 const clickHandler = () => {
    props.onclick(props.type)
  }

  let imageName = props.type.toLowerCase();
    return (
      <div>
        <Card onClick={() => clickHandler()}>
          <img src={"/assets/images/"+imageName+".jpg"} alt={props.type} height="300" width="250" />
          <CardBody>
            <CardLink><strong>{props.type}</strong></CardLink>
          </CardBody>
        </Card>
      </div>
    );

  }


export default CardTemplate;