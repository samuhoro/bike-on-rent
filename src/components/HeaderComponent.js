import React from 'react';
import {
    Navbar,
    NavbarBrand,
    Nav,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem, Button, Modal, ModalHeader, ModalBody, Form, FormGroup, Label, Input
} from 'reactstrap';
import { NavLink } from 'react-router-dom';

export default class Header extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoginModalOpen: false,
            isSignUpModalOpen: false,
            isLogin: false,

            username: "",
            password: "",
            email: "",
            mobileNo: "",
            formErrors: { username: "", password: "", email: "", mobileNo: "" },
            emailValid: false,
            usernameValid: false,
            passwordValid: false,
            mobileNoValid: false,
            formValid: false
        }
    }



    toggle() {
        this.setState({
            isOpen: !this.state.isOpen,
        });
    }

    toggleLoginModal = () => {
        this.setState({
            isLoginModalOpen: !this.state.isLoginModalOpen,
        })
    }

    toggleSignUpModal = () => {
        if (this.state.isLoginModalOpen) {
            this.toggleLoginModal();
        }
        this.setState({
            isSignUpModalOpen: !this.state.isSignUpModalOpen,
            formValid: false
        })
        alert(this.state.formValid);
    }

    handleLogin = () => {
        this.toggleLoginModal();
        alert("Username : " + this.username.value + " Password : " + this.password.value + " Remember : " + this.remember.checked);

    }

    handleSignUp = () => {
        this.toggleSignUpModal();
        alert("Username : " + this.state.username + " Password : " + this.state.password + " Email : " + this.state.email + " Mobile No : " + this.state.mobileNo);
        
    }

    inputHandler = (event) => {
        const name = event.target.name;
        const value = event.target.value;

        this.setState({ [name]: value }, () => { this.validateField(name, value) })
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;
        let usernameValid = this.state.usernameValid;
        let mobileNoValid = this.state.mobileNoValid;

        switch (fieldName) {
            case 'email':
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                fieldValidationErrors.email = emailValid ? '' : ' is invalid.';
                break;
            case 'password':
                passwordValid = value.length >= 6;
                fieldValidationErrors.password = passwordValid ? '' : ' is too short.';
                break;
            case 'username':
                usernameValid = value.length >= 6;
                fieldValidationErrors.username = usernameValid ? '' : ' is too short.'
                break;
            case 'mobileNo':
                mobileNoValid = !isNaN(Number(value));
                fieldValidationErrors.mobileNo = mobileNoValid ? '' : ' only number allowed.'
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            emailValid: emailValid,
            passwordValid: passwordValid,
            usernameValid: usernameValid,
            mobileNoValid: mobileNoValid
        }, this.validateForm);

    }

    validateForm=()=> {
        this.setState({
            formValid: this.state.emailValid && this.state.passwordValid && this.state.usernameValid && this.state.mobileNoValid
        })
    }

    routeHandler = () => {
        this.props.history.push("/home");
    }

    render() {
        const authControls = this.state.isLogin ? <DropdownItem>
            <span className="fa fa-sign-out fa-lg" onClick={this.toggleLogoutModal}></span>  Logout
    </DropdownItem> : <DropdownItem onClick={this.toggleLoginModal}>
                <span className="fa fa-sign-in fa-lg" ></span>  Login
    </DropdownItem>;


        return (
            <div>
                <Navbar dark expand="md">
                    <NavbarBrand className="mr-auto"><img src='/assets/images/logo.png' onClick={this.routeHandler} height="43" width="47" alt='Bike On Rent' /></NavbarBrand>

                    <h2 className="textCenter">Bikes On Rent</h2>
                    <Nav className="ml-auto" navbar>
                        <UncontrolledDropdown>
                            <DropdownToggle nav>
                                <span className="fa fa-bars fa-2x"></span>
                            </DropdownToggle>
                            <DropdownMenu right>
                                <DropdownItem>
                                    <NavLink to="/bikeRepair">
                                        <span className="fa fa-wrench fa-lg"></span> Bike Repair Services
                                     </NavLink>
                                </DropdownItem>
                                <DropdownItem>
                                    <NavLink to="/cityGuide">
                                        <span className="fa fa-compass fa-lg"></span> City Guide
                                     </NavLink>
                                </DropdownItem>
                                <DropdownItem divider />
                                <DropdownItem>
                                    <NavLink to="/firstAid">
                                        <span className="fa fa-medkit fa-lg"></span> First Aid Service
                                     </NavLink>
                                </DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                        <UncontrolledDropdown  >
                            <DropdownToggle nav >
                                <span className="fa fa-user-circle fa-2x"></span>
                            </DropdownToggle>
                            <DropdownMenu right>
                                <DropdownItem>
                                    <NavLink to="/profile">
                                        <span className="fa fa-user-circle fa-lg"></span> Your Profile
                                     </NavLink>
                                </DropdownItem>
                                <DropdownItem>
                                    <NavLink to="/bookingDetails">
                                        <span className="fa fa-info-circle fa-lg"></span> Booking Details
                                     </NavLink>
                                </DropdownItem>
                                <DropdownItem>
                                    <NavLink to="/feedback">
                                        <span className="fa fa-comments fa-lg"></span> Add Feedback
                                     </NavLink>
                                </DropdownItem>
                                <DropdownItem divider />
                                {authControls}
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </Nav>
                </Navbar>

                {/* login modal */}
                <React.Fragment>
                    <Modal isOpen={this.state.isLoginModalOpen} toggle={this.toggleLoginModal}>
                        <ModalHeader><img src='/assets/images/logo.png' height="43" width="47" alt='Bike On Rent' />Sign In</ModalHeader>
                        <ModalBody>
                            <Form onSubmit={this.handleLogin}>
                                <FormGroup>
                                    <Label htmlFor="username">Username</Label>
                                    <Input type="text" id="username" name="username" placeholder="User Name"
                                        innerRef={(input) => this.username = input} />
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="password">Password</Label>
                                    <Input type="password" id="password" name="password" placeholder="Password"
                                        innerRef={(input) => this.password = input} />
                                </FormGroup>
                                <Label style={{ color: 'blue' }} onClick={this.toggleSignUpModal}>Forgot Password?</Label>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox" name="remember"
                                            innerRef={(input) => this.remember = input} />
                                        Remember me
                                </Label>
                                </FormGroup>
                                <Button type="submit" value="submit" color="primary">Login</Button>{'  '}

                                <Button color="secondary" onClick={this.toggleSignUpModal} className="mr-auto">Create Account</Button>
                            </Form>
                        </ModalBody>
                    </Modal>
                </React.Fragment>

                {/* Sign-up modal */}
                <React.Fragment>
                    <Modal isOpen={this.state.isSignUpModalOpen} toggle={this.toggleSignUpModal}>
                        <ModalHeader><img src='/assets/images/logo.png' height="43" width="47" alt='Bike On Rent' />Sign Up</ModalHeader>
                        <ModalBody>
                            <Form>
                                <FormGroup>
                                    <Label htmlFor="username">Username</Label>

                                    <Input type="text" name="username" placeholder="User Name"
                                        onChange={(event) => this.inputHandler(event)} />
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="email">Email Id</Label>

                                    <Input type="text" name="email" placeholder="Email Id"
                                        onChange={(event) => this.inputHandler(event)} />
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="password">Password</Label>
                                    <Input type="password" name="password" placeholder="Password"
                                        onChange={(event) => this.inputHandler(event)} />
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="mobileNo">Mobile Number</Label>
                                    <Input type="text" name="mobileNo" placeholder="Mobile Number"
                                        onChange={(event) => this.inputHandler(event)} />
                                </FormGroup>

                                <Button color="primary" disabled={!this.state.formValid} onClick={this.handleSignUp}>Create Account</Button>
                            </Form>
                        </ModalBody>
                    </Modal>
                </React.Fragment>
            </div >
        );
    }
}

