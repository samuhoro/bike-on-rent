import React from 'react';
import { Card, CardTitle, CardBody } from 'reactstrap';


const BikeCard = (props) => {

  const clickHandler = () => {
    props.onclick(props.bike.id);
  }

  const bookHandler=()=>{
    props.routeToBooking(props.bike.id)
  }

  return (
    <div className="col-sm-3 mt-4">
      <Card>
        <CardTitle className="textCenter">{props.bike.bikeModel}</CardTitle>
        <img src={"/assets/images/bikes/" + props.bike.imageList[props.bike.id - 1]} onClick={() => clickHandler()} alt={props.bike.bikeModel} height="300" width="250" />
        <CardBody className="textCenter">
          <strong>{props.bike.price + " Rs/Day"}</strong>
          <button className="ml-2 btn btn-primary" onClick={()=>bookHandler()}>Book</button>
        </CardBody>
      </Card>
    </div>
  );

}


export default BikeCard;