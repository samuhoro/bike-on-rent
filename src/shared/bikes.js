export const BikeList = [{
    id: 1,
    bikeModel: "Bajaj Avenger",
    price: "1000",
    details: {
        registration: "JH01 2023",
        purchaseYear: "2017",
        mileage: "59",
        lastServicing: "12 june 2018",
        cc: "150",
        km: "10000"
    },
     imageList: ["1.jpg" , "2.jpg" , "3.jpg","4.jpg" , "5.jpg" , "6.jpg", "7.jpg", "8.jpg" ]
},
{
    id: 2,
    bikeModel: "Bajaj Avenger",
    price: "1200",
    details: {
        registration: "JH01 8938",
        purchaseYear: "2016",
        mileage: "45",
        lastServicing: "05 sep 2018",
        cc: "220",
        km: "2000"
    },
     imageList: ["1.jpg" , "2.jpg" , "3.jpg","4.jpg" , "5.jpg" , "6.jpg", "7.jpg", "8.jpg" ]
}, {
    id: 3,
    bikeModel: "Bajaj Avenger",
    price: "1200",
    details: {
        registration: "JH01 8627",
        purchaseYear: "2018",
        mileage: "59",
        lastServicing: "21 aug 2018",
        cc: "220",
        km: "1000"
    },
      imageList: ["1.jpg" , "2.jpg" , "3.jpg","4.jpg" , "5.jpg" , "6.jpg", "7.jpg", "8.jpg" ]
}, {
    id: 4,
    bikeModel: "KTM",
    price: "1100",
    details: {
        registration: "JH01 1123",
        purchaseYear: "2017",
        mileage: "59",
        lastServicing: "12 dec 2017",
        cc: "200",
        km: "6000"
    },
      imageList: ["1.jpg" , "2.jpg" , "3.jpg","4.jpg" , "5.jpg" , "6.jpg", "7.jpg", "8.jpg" ]
}, {
    id: 5,
    bikeModel: "Royal Enfield",
    price: "1300",
    details: {
        registration: "JH01 2823",
        purchaseYear: "2017",
        mileage: "30",
        lastServicing: "12 june 2018",
        cc: "350",
        km: "6000"
    },
      imageList: ["1.jpg" , "2.jpg" , "3.jpg","4.jpg" , "5.jpg" , "6.jpg", "7.jpg", "8.jpg" ]
}, {
    id: 6,
    bikeModel: "Scooty",
    price: "800",
    details: {
        registration: "JH01 2023",
        purchaseYear: "2017",
        mileage: "50",
        lastServicing: "12 june 2018",
        cc: "150",
        km: "10000"
    },
      imageList: ["1.jpg" , "2.jpg" , "3.jpg","4.jpg" , "5.jpg" , "6.jpg", "7.jpg", "8.jpg" ]
},{
    id: 7,
    bikeModel: "Bajaj Pulser",
    price: "1000",
    details: {
        registration: "JH01 9923",
        purchaseYear: "2017",
        mileage: "60",
        lastServicing: "12 jan 2018",
        cc: "150",
        km: "1000"
    },
      imageList: ["1.jpg" , "2.jpg" , "3.jpg","4.jpg" , "5.jpg" , "6.jpg", "7.jpg", "8.jpg" ]
}, {
    id: 8,
    bikeModel: "Activa",
    price: "800",
    details: {
        registration: "JH01 1023",
        purchaseYear: "2017",
        mileage: "50",
        lastServicing: "12 june 2018",
        cc: "150",
        km: "7000"
    },
      imageList: ["1.jpg" , "2.jpg" , "3.jpg","4.jpg" , "5.jpg" , "6.jpg", "7.jpg", "8.jpg" ]
}]