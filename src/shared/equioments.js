export const EquipmentList = [
    {
        id:1,
        name:"Gloves",
        price: 250,
        details: "abc",
        images : ["1.jpg","2.jpg","3.jpg","4.jpg","5.jpg","6.jpg","7.jpg","8.jpg"]
    },
    {
        id:2,
        name:"Jackets",
        price: 600,
        details: "abc",
        images : ["1.jpg","2.jpg","3.jpg","4.jpg","5.jpg","6.jpg","7.jpg","8.jpg"]
    },
    {
        id:3,
        name:"Helmets",
        price: 200,
        details: "abc",
        images : ["1.jpg","2.jpg","3.jpg","4.jpg","5.jpg","6.jpg","7.jpg","8.jpg"]
    },
    {
        id:4,
        name:"Footwear",
        price: 200,
        details: "abc",
        images : ["1.jpg","2.jpg","3.jpg","4.jpg","5.jpg","6.jpg","7.jpg","8.jpg"]
    },
    {
        id:5,
        name:"Riding Headwear",
        price: 100,
        details: "abc",
        images : ["1.jpg","2.jpg","3.jpg","4.jpg","5.jpg","6.jpg","7.jpg","8.jpg"]
    },
    {
        id:6,
        name:"Base Layers and Liners",
        price: 150,
        details: "abc",
        images : ["1.jpg","2.jpg","3.jpg","4.jpg","5.jpg","6.jpg","7.jpg","8.jpg"]
    },
    {
        id:7,
        name:"Racesuits",
        price: 500,
        details: "abc",
        images : ["1.jpg","2.jpg","3.jpg","4.jpg","5.jpg","6.jpg","7.jpg","8.jpg"]
    },
    {
        id:8,
        name:"Protective Gear",
        price: 200,
        details: "abc",
        images : ["1.jpg","2.jpg","3.jpg","4.jpg","5.jpg","6.jpg","7.jpg","8.jpg"]
    }
]