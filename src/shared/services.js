export const SERVICES = [
    {
        id: 1,
        routeId : 1,
        repairShopName: "ABC Bike Repair Center",
        timing: "9:00 AM to 6:00 PM",
        address: "Adesh nagaer Ranchi",
        contact: 9839849303
    },
    {
        id: 2,
        routeId : 1,
        repairShopName: "Good Bike Repair Center",
        timing: "9:00 AM to 6:00 PM",
        address: "Adesh nagaer Ranchi",
        contact: 9839849303
    },
    {
        id: 3,
        routeId : 1,
        repairShopName: "Bike Repair Center",
        timing: "9:00 AM to 6:00 PM",
        address: "Adesh nagaer Ranchi",
        contact: 9839849303
    },
    {
        id: 4,
        routeId : 2,
        repairShopName: "Raj Bike Repair",
        timing: "9:00 AM to 6:00 PM",
        address: "Adesh nagaer Ranchi",
        contact: 9938728291
    },
    {
        id: 5,
        routeId : 2,
        repairShopName: "Jay Bike Repair Center",
        timing: "9:00 AM to 6:00 PM",
        address: "Adesh nagaer Ranchi",
        contact: 9092837418
    },
    {
        id: 6,
        routeId : 3,
        repairShopName: "Bike Repair Center",
        timing: "9:00 AM to 6:00 PM",
        address: "Adesh nagaer Ranchi",
        contact: 9839849303
    },
    {
        id: 7,
        routeId : 3,
        repairShopName: "Jay Bike Repair Center",
        timing: "9:00 AM to 6:00 PM",
        address: "Adesh nagaer Ranchi",
        contact: 9092837418
    },
    {
        id: 8,
        routeId : 3,
        repairShopName: "Vijay Bike Repair Center",
        timing: "8:00 AM to 5:00 PM",
        address: "Adesh nagaer Ranchi",
        contact: 8934894948
    },
    {
        id: 9,
        routeId : 4,
        repairShopName: "Ram Bike Repair Center",
        timing: "10:00 AM to 4:00 PM",
        address: "Adesh nagaer Ranchi",
        contact: 9832377328
    },
    {
        id: 10,
        routeId : 4,
        repairShopName: "Jay Bike Repair Center",
        timing: "9:00 AM to 6:00 PM",
        address: "Adesh nagaer Ranchi",
        contact: 9092837418
    },
    {
        id: 11,
        routeId : 4,
        repairShopName: "Vijay Bike Repair Center",
        timing: "8:00 AM to 5:00 PM",
        address: "Adesh nagaer Ranchi",
        contact: 8934894948
    }

]